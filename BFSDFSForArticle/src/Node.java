/**
 * Class models the a graph node/vertex
 */
public class Node {
    public char label;
    public boolean visited = false;

    public Node(char label) {
        this.label = label;
    }
}
