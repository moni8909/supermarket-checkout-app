import java.util.LinkedList;

public class Main {

    public static void main(String[] args) {

        Node nA = new Node('A');
        Node nB = new Node('B');
        Node nC = new Node('C');
        Node nD = new Node('D');
        Node nE = new Node('E');
        Node nF = new Node('F');

        Graph graph = new Graph();
        graph.addNode(nA);
        graph.addNode(nB);
        graph.addNode(nC);
        graph.addNode(nD);
        graph.addNode(nE);
        graph.addNode(nF);
        graph.setRootNode(nA);

        graph.connectTwoNodes(nA, nB);
        graph.connectTwoNodes(nA, nC);
        graph.connectTwoNodes(nA, nD);

        graph.connectTwoNodes(nB, nE);
        graph.connectTwoNodes(nB, nF);
        graph.connectTwoNodes(nC, nF);

        System.out.println("DFS Traversal of a tree is ------------->");
        graph.depthFirstSearch();

        System.out.println("\nBFS Traversal of a tree is ------------->");
        graph.breadthFirstSearch();
    }

    /**
     * Reversing a linked list in place
     * @param original
     * @param <T>
     * @return
     */
    public static <T> CustomLinkedList<T> reverse(final CustomLinkedList<T> original) {

        if (original == null) {
            throw new NullPointerException("Cannot reverse a null list");
        }

        if (original.getNext() == null) {
            return original;
        }

        final CustomLinkedList<T> next = original.getNext();
        original.setNext(null);

        final CustomLinkedList<T> othersReversed = reverse(next);

        next.setNext(original);
        return othersReversed;
    }
}

