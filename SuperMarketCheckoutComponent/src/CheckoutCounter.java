import sun.security.krb5.internal.ktab.KeyTabEntry;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by J000088 on 07/01/2016.
 */
public class CheckoutCounter {

    private Supermarket supermarket;

    private List listOfItems;
    private List offers;

    public CheckoutCounter() {
        listOfItems = new ArrayList<Item>();

        offers = new ArrayList<Offer>();

        supermarket = new Supermarket(listOfItems, offers);

        supermarket.createAndApplyAnewOfferToItem();
    }

    public void scanItems(Item... items) {
        if (items.length > 1) {
            listOfItems = Arrays.asList(items);
            calculateTotalCost(listOfItems);
        }
    }

    private double calculateTotalCost(List<? extends Item> listOfItems) {
        return listOfItems.stream().mapToDouble(Item::getPrice).sum();
    }
}
