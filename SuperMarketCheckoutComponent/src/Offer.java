import java.util.ArrayList;
import java.util.List;

/**
 * Created by J000088 on 07/01/2016.
 */
public class Offer {
    private List items;
    private double promotionalPrice;

    public Offer(double promotionalPrice) {
        items = new ArrayList<Item>();
        this.promotionalPrice = promotionalPrice;
    }

    public List getItems() {
        return items;
    }

    public void setItems(List items) {
        this.items = items;
    }

    public double getPromotionalPrice() {
        return promotionalPrice;
    }

    public void setPromotionalPrice(double promotionalPrice) {
        this.promotionalPrice = promotionalPrice;
    }
}
