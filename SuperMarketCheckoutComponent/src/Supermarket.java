import java.util.ArrayList;
import java.util.List;

/**
 * Created by J000088 on 07/01/2016.
 */
public class Supermarket {
    private List items;
    private List offers;

    public Supermarket(List items, List offers) {
        this.items = new ArrayList<Item>();
        this.offers = new ArrayList<Offer>();
    }

    public List getItems() {
        return items;
    }

    public void setItems(List items) {
        this.items = items;
    }

    public void createAndApplyAnewOfferToItem(Item item, int minNumberOfItemsToBePurchased, double totalPrice) {
        double promotionalPricePerItem = totalPrice / minNumberOfItemsToBePurchased;
        item.setOnOffer(true);
        item.setPrice(promotionalPricePerItem);
    }

    public List getOffers() {
        return offers;
    }

    public void setOffers(List offers) {
        this.offers = offers;
    }
}
