/**
 * Created by J000088 on 07/01/2016.
 */
public class Item {

    private String label;
    private double price;
    private boolean onOffer;

    public Item(String label, double price, boolean onOffer) {
        this.label = label;
        this.price = price;
        this.onOffer = onOffer;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isOnOffer() {
        return onOffer;
    }

    public void setOnOffer(boolean onOffer) {
        this.onOffer = onOffer;
    }
}
